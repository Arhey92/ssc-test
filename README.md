- Comments
    - Create new comment
    - Show comment
    - Delete comment
    - Like/Unlike comment
- User
    - Show user
- News
    - Like/Unlike news

## Comments
### Create new comment
Параметры запроса:
```text
Method: POST
URL: http://{server-site-url}/comments/new
Headers: {
    Content-Type: application/json
}
```
Тело запроса(form-data):
```json
{
	"news_id": 1,
	"parent_comment_id": 1,
	"text": "Another comment"
}
```

Ответ сервера должен быть следующим(`Status: 201 Created`):
```json
{
    "status": "success",
    "error_message": "",
    "data": {
        "comment": true
    }
}
```

### Show comment
Параметры запроса:
```text
Method: GET
URL: http://{server-site-url}/comments/{comment_id}
Headers: {
    Content-Type: application/json
}
```

Ответ сервера должен быть следующим(`Status: 200 OK`):
```json
{
    "status": "success",
    "error_message": "",
    "data": {
        "comment": {
            "id": "1",
            "news_id": "1",
            "parent_comment_id": null,
            "text": "new comment 1",
            "time_created": "2019-03-24 04:11:36"
        }
    }
}
```

### Delete comment
Параметры запроса:
```text
Method: DELETE
URL: http://{server-site-url}/comments/{comment_id}
Headers: {
    Content-Type: application/json
}
```

Ответ сервера должен быть следующим(`Status: 204 No Content`):
```json
```

### Like/Unlike comment
##### Action can be 1(like) and 0(unlike)
Параметры запроса:
```text
Method: POST
URL: http://{server-site-url}/comment_likes/action
Headers: {
    Content-Type: application/json
}
```
Тело запроса(form-data):
```json
{
	"user_id": 2,
	"comment_id": 3,
	"action": 1
}
```

Ответ сервера должен быть следующим(`Status: 200 OK`):
```json
{
    "status": "success",
    "error_message": "",
    "data": {
        "comment": true
    }
}
```

## Users
### Show user
Параметры запроса:
```text
Method: GET
URL: http://{server-site-url}/users/{user_id}
Headers: {
    Content-Type: application/json
}
```

Ответ сервера должен быть следующим(`Status: 200 OK`):
```json
{
    "status": "success",
    "error_message": "",
    "data": {
        "user": {
            "id": "3",
            "name": "Brian",
            "time_created": "2019-03-16 00:15:23"
        }
    }
}
```

## News
### Like/Unlike news
Параметры запроса:
```text
Method: POST
URL: http://{server-site-url}/news_likes/action
Headers: {
    Content-Type: application/json
}
```
Тело запроса(form-data):
```json
{
	"user_id": 2,
	"news_id": 2,
	"action": 1
}
```

Ответ сервера должен быть следующим(`Status: 200 OK`):
```json
{
    "status": "success",
    "error_message": "",
    "data": {
        "news": true
    }
}
```