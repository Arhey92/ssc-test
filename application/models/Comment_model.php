<?php
/**
 * Created by PhpStorm.
 * User: Arhey
 * Date: 23.03.2019
 * Time: 22:53
 */

class Comment_model extends MY_Model
{
    const COMMENT_TABLE = APPLICATION_COMMENTS;

    protected $id;
    protected $parent_comment_id;
    protected $text;
    protected $time_created;

    function __construct($id = FALSE)
    {
        parent::__construct();
        $this->class_table = self::COMMENT_TABLE;
        $this->set_id($id);
    }

    /**
     * @return string
     */
    public function get_parent_comment_id()
    {
        return $this->parent_comment_id;
    }

    /**
     * @param int $parent_comment_id
     */
    public function set_parent_comment_id(int $parent_comment_id)
    {
        $this->parent_comment_id = $parent_comment_id;
        return $this->_save('parent_comment_id', $parent_comment_id);
    }

    /**
     * @return mixed
     */
    public function get_time_created()
    {
        return $this->time_created;
    }

    /**
     * @param mixed $time_created
     */
    public function set_time_created($time_created)
    {
        $this->time_created = $time_created;
        return $this->_save('time_created', $time_created);
    }

    /**
     * @return mixed
     */
    public function get_text()
    {
        return $this->text;
    }

    /**
     * @param mixed $text
     */
    public function set_text($text)
    {
        $this->text = $text;
        return $this->_save('text', $text);
    }

    public static function get_comment(int $comment_id)
    {
        $CI =& get_instance();

        $_data = $CI->s->from(self::COMMENT_TABLE)->where(['id' => $comment_id])->one();

        return $_data;
    }

    public static function create($data)
    {
        $CI =& get_instance();

        $data['time_created'] = date('Y-m-d H:i:s');
        $_data = $CI->s->from(self::COMMENT_TABLE)->insert($data)->execute();

        return $_data;
    }

    public static function delete(int $comment_id)
    {
        $CI =& get_instance();

        $_data = $CI->s->from(self::COMMENT_TABLE)->where(['id' => $comment_id])->delete()->execute();

        return $_data;
    }
}