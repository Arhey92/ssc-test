<?php
/**
 * Created by PhpStorm.
 * User: Arhey
 * Date: 23.03.2019
 * Time: 22:53
 */

class News_like_model extends MY_Model
{
    const NEWS_LIKE_TABLE = APPLICATION_NEWS_LIKES;

    protected $id;
    protected $news_id;
    protected $user_id;

    function __construct($id = FALSE)
    {
        parent::__construct();
        $this->class_table = self::NEWS_LIKE_TABLE;
        $this->set_id($id);
    }

    /**
     * @return int
     */
    public function get_news_id()
    {
        return $this->news_id;
    }

    /**
     * @param int $news_id
     */
    public function set_news_id(int $news_id)
    {
        $this->news_id = $news_id;
        return $this->_save('news_id', $news_id);
    }

    /**
     * @return int
     */
    public function get_user_id()
    {
        return $this->user_id;
    }

    /**
     * @param int $user_id
     */
    public function set_user_id($user_id)
    {
        $this->user_id = $user_id;
        return $this->_save('user_id', $user_id);
    }

    public static function update($data)
    {
        $CI =& get_instance();

        $exist_news = $CI->s->from(self::NEWS_LIKE_TABLE)->where('news_id', $data['news_id'])->where('user_id', $data['user_id'])->count();

        if(!$exist_news){
            $_data = $CI->s->from(self::NEWS_LIKE_TABLE)->insert($data)->execute();
        }else{
            $_data = $CI->s->from(self::NEWS_LIKE_TABLE)->where(['news_id' => $data['news_id']])->where(['user_id' => $data['user_id']])->update($data)->execute();
        }

        return $_data;
    }
}