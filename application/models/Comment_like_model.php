<?php
/**
 * Created by PhpStorm.
 * User: Arhey
 * Date: 23.03.2019
 * Time: 22:53
 */

class Comment_like_model extends MY_Model
{
    const COMMENT_LIKE_TABLE = APPLICATION_COMMENT_LIKES;

    protected $id;
    protected $comment_id;
    protected $user_id;

    function __construct($id = FALSE)
    {
        parent::__construct();
        $this->class_table = self::COMMENT_LIKE_TABLE;
        $this->set_id($id);
    }

    /**
     * @return int
     */
    public function get_comment_id()
    {
        return $this->comment_id;
    }

    /**
     * @param int $comment_id
     */
    public function set_comment_id(int $comment_id)
    {
        $this->comment_id = $comment_id;
        return $this->_save('comment_id', $comment_id);
    }

    /**
     * @return int
     */
    public function get_user_id()
    {
        return $this->user_id;
    }

    /**
     * @param int $user_id
     */
    public function set_user_id($user_id)
    {
        $this->user_id = $user_id;
        return $this->_save('user_id', $user_id);
    }

    public static function update($data)
    {
        $CI =& get_instance();

        $exist_comment = $CI->s->from(self::COMMENT_LIKE_TABLE)->where('comment_id', $data['comment_id'])->where('user_id', $data['user_id'])->count();

        if(!$exist_comment){
            $_data = $CI->s->from(self::COMMENT_LIKE_TABLE)->insert($data)->execute();
        }else{
            $_data = $CI->s->from(self::COMMENT_LIKE_TABLE)->where(['comment_id' => $data['comment_id']])->where(['user_id' => $data['user_id']])->update($data)->execute();
        }

        return $_data;
    }
}