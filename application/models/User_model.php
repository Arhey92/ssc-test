<?php
/**
 * Created by PhpStorm.
 * User: Arhey
 * Date: 23.03.2019
 * Time: 22:53
 */

class User_model extends MY_Model
{
    const USER_TABLE = APPLICATION_USERS;

    protected $id;
    protected $name;
    protected $time_created;

    function __construct($id = FALSE)
    {
        parent::__construct();
        $this->class_table = self::USER_TABLE;
        $this->set_id($id);
    }

    /**
     * @return string
     */
    public function get_name()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function set_name($name)
    {
        $this->name = $name;
        return $this->_save('name', $name);
    }

    /**
     * @return mixed
     */
    public function get_time_created()
    {
        return $this->time_created;
    }

    /**
     * @param mixed $time_created
     */
    public function set_time_created($time_created)
    {
        $this->time_created = $time_created;
        return $this->_save('time_created', $time_created);
    }

    public static function get_user(int $user_id)
    {
        $CI =& get_instance();

        $_data = $CI->s->from(self::USER_TABLE)->where(['id' => $user_id])->one();

        return $_data;
    }
}