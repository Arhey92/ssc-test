<?php

class Comment_likes extends MY_Controller
{
    protected $response_data;

    public function __construct()
    {
        parent::__construct();

        $this->CI =& get_instance();
        $this->load->model('comment_like_model');

        $this->response_data = new stdClass();
        $this->response_data->status = 'success';
        $this->response_data->error_message = '';
        $this->response_data->data = new stdClass();

        if (ENVIRONMENT === 'production')
        {
            die('Access denied!');
        }
    }

    public function comment_action()
    {
        $this->response_data->data->comment = Comment_like_model::update($this->input->post());
        $this->response($this->response_data, 200);
    }
}
