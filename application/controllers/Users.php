<?php

class Users extends MY_Controller
{
    protected $response_data;
    protected $user_id;

    public function __construct()
    {
        parent::__construct();

        $this->CI =& get_instance();
        $this->load->model('user_model');

        $this->response_data = new stdClass();
        $this->response_data->status = 'success';
        $this->response_data->error_message = '';
        $this->response_data->data = new stdClass();

        if (ENVIRONMENT === 'production')
        {
            die('Access denied!');
        }
    }

    public function user_get($user_id)
    {
        $this->response_data->data->user = User_model::get_user($user_id);
        $this->response($this->response_data);
    }
}
