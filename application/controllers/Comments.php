<?php

class Comments extends MY_Controller
{
    protected $response_data;

    public function __construct()
    {
        parent::__construct();

        $this->CI =& get_instance();
        $this->load->model('comment_model');

        $this->response_data = new stdClass();
        $this->response_data->status = 'success';
        $this->response_data->error_message = '';
        $this->response_data->data = new stdClass();

        if (ENVIRONMENT === 'production')
        {
            die('Access denied!');
        }
    }

    public function comment_show($comment_id)
    {
        $this->response_data->data->comment = Comment_model::get_comment($comment_id);
        $this->response($this->response_data);
    }

    public function comment_create()
    {
        $this->response_data->data->comment = Comment_model::create($this->input->post());
        $this->response($this->response_data, 201);
    }

    public function comment_delete($comment_id)
    {
        $this->response_data->data->comment = Comment_model::delete($comment_id);
        $this->response([], 204);
    }
}
